# stb_dxt rust bindings

## License
Bindings licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

`stb_dxt` licensed under either of
 * MIT License (Expat) (http://opensource.org/licenses/MIT)
 * Public Domain (www.unlicense.org)

at your option.
