pub const STB_DXT_NORMAL: i32 = 0;

/// use dithering. dubious win. never use for normal maps and the like!
pub const STB_DXT_DITHER: i32 = 1;

/// high quality mode, does two refinement steps instead of 1. ~30-40% slower.
pub const STB_DXT_HIGHQUAL: i32 = 2;

extern {
    pub fn stb_compress_dxt_block(dest: *mut u8, src_rgba_four_bytes_per_pixel: *const u8, alpha: i32, mode: i32);
    pub fn stb_compress_bc4_block(dest: *mut u8, src_r_one_byte_per_pixel: *const u8);
    pub fn stb_compress_bc5_block(dest: *mut u8, src_rg_two_byte_per_pixel: *const u8);
}
