extern crate stb_dxt;

fn main() {
    // 4x4 block of RGBA pixels
    let src: [u8; 4*4*4] = [
        255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255,
        255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255,
        0, 255, 0, 255, 0, 255, 0, 255, 0, 0, 255, 255, 0, 0, 255, 255,
        0, 255, 0, 255, 0, 255, 0, 255, 0, 0, 255, 255, 0, 0, 255, 255,
    ];

    // BC1 Block
    let mut dest: [u8; 8] = [0; 8];

    unsafe {
        stb_dxt::stb_compress_dxt_block(
            dest.as_mut_ptr(),
            src.as_ptr(),
            0,
            stb_dxt::STB_DXT_NORMAL,
        );
    }

    println!("Result: {:?}", dest);
}
