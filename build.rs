extern crate cc;

fn main() {
    cc::Build::new()
        .file("src/stb_dxt.c")
        .compile("stb_dxt");
}
